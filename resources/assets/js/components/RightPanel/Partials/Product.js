import React, {Component} from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {showDetailPost} from '../../../actions/index'

class Product extends Component {

    showDetailPost() {
        const {title, imagesource, content, price, showDetailPost} = this.props
        showDetailPost({title: title, thumb: imagesource, content: content, price: price})
        $('#detail_post').modal('show')
    }

    render() {
        const {title, imagesource, content, price} = this.props
        return (
            <div className="col-sm-6 col-md-4">
                <div className="thumbnail">
                    <img src={imagesource} alt={title}/>
                    <div className="caption">
                        <h3>{title}</h3>
                        <p>
                            {content}
                        </p>
                        <p>
                            Price: {price}
                        </p>
                        <p>
                            <a onClick={(e)=>{e.preventDefault(); this.showDetailPost()}} href="#"
                               className="btn btn-primary">Detail</a>
                        </p>
                    </div>
                </div>
            </div>
        )
    }
}


function mapDispatchToProps(dispatch) {
    return bindActionCreators({showDetailPost}, dispatch)
}


export default connect(null, mapDispatchToProps)(Product)