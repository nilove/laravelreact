import React, {Component} from 'react'
import Product from './Partials/Product'
import LoadingWidget from '../../widgets/LoadingWidget'
import PostCreate from '../../widgets/Modals/PostCreate'
import PostDetail from '../../widgets/Modals/PostDetail'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {fetchPosts, changeLoading} from '../../actions/index'
import debounce from 'lodash/debounce'
import Masonry from 'react-masonry-component'

class RightPanel extends Component {

    componentDidMount() {
        const {fetchPosts} = this.props
        fetchPosts(1)
        this.handlePagination()
    }


    handlePagination() {
        window.onscroll = debounce(function () {
            if (this.getScrollTop() < this.getDocumentHeight() - window.innerHeight)
                return
            const {fetchPosts, pagination, changeLoading} = this.props

            if (pagination.get('current_page') > 0 && pagination.get('current_page') < pagination.get('last_page')) {
                changeLoading(true)
                fetchPosts(pagination.get('current_page') + 1)
            }

        }.bind(this), 1000)
    }


    getScrollTop() {
        return (window.pageYOffset !== undefined) ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop;
    }

    getDocumentHeight() {
        const body = document.body;
        const html = document.documentElement;

        return Math.max(
            body.scrollHeight, body.offsetHeight,
            html.clientHeight, html.scrollHeight, html.offsetHeight
        )
    }


    renderPosts() {
        const {posts} = this.props
        if (!posts)
            return null
        return posts.map((post)=> {
            return <Product key={post.get('id')} title={post.get('title')} imagesource={post.get('thumb')}
                            content={post.get('content')} price={post.get('price')}/>
        })
    }


    renderLoading() {
        const {loading} = this.props
        if (loading)
            return <LoadingWidget />

        else return null

    }

    render() {
        return (
            <div className="col-md-9">
                <div className="panel panel-default">
                    <div className="panel-heading">Posts</div>
                    <div className="panel-body">
                        <Masonry>
                            {this.renderPosts()}
                        </Masonry>
                        {this.renderLoading()}
                        
                        <PostCreate />
                        <PostDetail />
                    </div>
                </div>
            </div>
        )
    }
}


function mapDispatchToProps(dispatch) {
    return bindActionCreators({fetchPosts, changeLoading}, dispatch)
}

function mapStateToProps(state) {
    return {
        posts: state.posts.getIn(['posts']),
        pagination: state.posts.getIn(['pagination']),
        loading: state.posts.getIn(['loading'])
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RightPanel)