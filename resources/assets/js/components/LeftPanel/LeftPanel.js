import React, {Component} from 'react'
import Categories from './Partials/Categories'
import RangeSlider from './Partials/RangeSlider'
import FilterText from './Partials/FilterText'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {fetchCategories} from '../../actions/index'


class LeftPanel extends Component {

    componentDidMount() {
        const {fetchCategories} = this.props
        fetchCategories()
    }

    render() {
        let {categories} = this.props
        return (
            <div className="col-md-3">
                <div className="panel panel-default">
                    <div className="panel-heading">Categories</div>
                    <div className="panel-body">
                        <FilterText />
                        <Categories categories={categories}/>
                        <RangeSlider />
                    </div>
                </div>
            </div>
        )
    }
}


function mapDispatchToProps(dispatch) {
    return bindActionCreators({fetchCategories}, dispatch);
}

function mapStateToProps(state) {

    return {
        categories: state.categories.getIn(['categories'])
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(LeftPanel)