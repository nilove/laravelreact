import React, {Component} from 'react'
import slider from 'bootstrap-slider'
import {findDOMNode} from 'react-dom'
import debounce from 'lodash/debounce'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {setFilterPrice, fetchPosts, changeLoading} from '../../../actions/index'
import {toJS} from 'immutable'

class RangeSlider extends Component {

    componentDidMount() {
        const {fetchPosts, changeLoading} = this.props
        const node = findDOMNode(this.refs.range_slider)
        $(node).slider({
            formatter: function (value) {
                return 'Current value: ' + value;
            }
        })
        $(node).change(debounce(function () {
            changeLoading(true)
            fetchPosts(1, {
                text: $('#search_text').val(),
                category_id: $('#categories').val(),
                price: $('#price').val()
            })
        }, 500))
    }

    render() {
        const style = {marginTop: '20px', width: '100%'}
        return (
            <div className="form-group" style={style}>
                Filter by price interval: <b> 10</b><br/>
                <input id="price" style={style} ref="range_slider" type="text"
                       className="span12" value=""
                       data-slider-min="0" data-slider-max="9999"
                       data-slider-step="5"
                       data-slider-value="[1,9999]"
                />
            </div>
        )
    }
}


function mapDispatchToProps(dispatch) {
    return bindActionCreators({setFilterPrice, fetchPosts, changeLoading}, dispatch)
}

function mapStateToProps(state) {
    return {
        filter: state.filter
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RangeSlider)