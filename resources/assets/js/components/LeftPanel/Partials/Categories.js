import React, {Component} from 'react'
import Category from './Category'
import {findDOMNode} from 'react-dom'
import select2 from 'select2'
import debounce from 'lodash/debounce'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {setFilterCategory, fetchPosts, changeLoading} from '../../../actions/index'


class Categories extends Component {


    componentDidMount() {
        const {fetchPosts, changeLoading} = this.props
        $(findDOMNode(this.refs.select)).select2({placeholder: "Select a category"})
        $(findDOMNode(this.refs.select)).change(debounce(function () {
            changeLoading(true)
            fetchPosts(1, {
                text: $('#search_text').val(),
                category_id: $('#categories').val(),
                price: $('#price').val()
            })
        }, 1000))
    }


    renderCategories() {
        let {categories} = this.props
        if (!categories)
            return null
        return categories.map((category)=> {
            return <Category key={category.get('id')} id={category.get('id')} title={category.get('title')}
                             count={category.get('total_post')}/>
        })
    }

    render() {
        return (
            <select id="categories" className="js-example-basic-multiple form-control" ref="select" multiple="multiple">
                {this.renderCategories()}
            </select>
        )
    }
}


function mapDispatchToProps(dispatch) {
    return bindActionCreators({setFilterCategory, fetchPosts, changeLoading}, dispatch)
}

function mapStateToProps(state) {
    return {
        filter: state.filter
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Categories)

