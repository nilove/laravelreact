import React, {Component} from 'react'
import {findDOMNode} from 'react-dom'
import debounce from 'lodash/debounce'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {setFilterText, fetchPosts, changeLoading} from '../../../actions/index'
import {toArray} from 'immutable'


class FilterText extends Component {


    componentDidMount() {
        const {fetchPosts, changeLoading} = this.props
        $(findDOMNode(this.refs.search_text)).keyup(debounce(function () {
            changeLoading(true)
            fetchPosts(1, {
                text: $('#search_text').val(),
                category_id: $('#categories').val(),
                price: $('#price').val()
            })
        }, 1000))
    }

    render() {
        return (
            <div className="form-group">
                <input id="search_text" ref="search_text" type="text" className="form-control" placeholder="Search"/>
            </div>
        )
    }
}


function mapDispatchToProps(dispatch) {
    return bindActionCreators({setFilterText, fetchPosts, changeLoading}, dispatch)
}

function mapStateToProps(state) {
    return {
        filter: state.filter
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(FilterText)