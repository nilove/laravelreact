import React, {Component} from 'react'

class Category extends Component {
    render() {
        let {id,title} = this.props
        return (
            <option value={id}>
                {title.substring(0,30)}
            </option>
        )
    }
}

export default Category