
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');





import React from 'react'
import ReactDOM from 'react-dom'
import App from './containers/App'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import reducers from './reducers'
import promise from 'redux-promise'



const createStoreWithMiddleware = applyMiddleware(promise)(createStore)

function renderMyComponent(mount) {
    ReactDOM.render(
        <Provider store={createStoreWithMiddleware(reducers, window.devToolsExtension && window.devToolsExtension())}>
            <App/>
        </Provider>,
        mount
    )

}

const mount = document.getElementById('example')

renderMyComponent(mount)
