import React, { Component } from 'react'
import LeftPanel from '../components/LeftPanel/LeftPanel'
import RightPanel from '../components/RightPanel/RightPanel'

class App extends Component {
    render() {
        return (
            <div className="row">
                <LeftPanel />
                <RightPanel />
            </div>
        )
    }
}

export default App


