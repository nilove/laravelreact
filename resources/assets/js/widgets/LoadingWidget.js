import React, {Component} from 'react'

class LoadingWidget extends Component {
    render() {
        return (
            <div>
                <div className="clearfix"></div>
                <div className="thumbnail">
                    <img src="/images/flickr.svg"/>
                </div>
            </div>
        )
    }
}


export default LoadingWidget