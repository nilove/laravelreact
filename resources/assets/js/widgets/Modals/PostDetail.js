import React, {Component} from 'react'
import {connect} from 'react-redux'

class PostDetail extends Component {
    render() {
        const {post} = this.props
        const style = {
            width: '100%'
        }
        return (
            <div className="modal fade" id="detail_post" role="dialog">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button " className="close" data-dismiss="modal"
                                    aria-hidden="true">&times;</button>
                            <h4 className="modal-title">
                                Post Detail
                            </h4>
                        </div>
                        <div className="modal-body">
                            <div className="col-sm-12 col-md-12">
                                <div className="thumbnail">
                                    <div className="min_image_height">
                                        <img style={style} src={post.get('thumb')} alt={post.get('title')}/>
                                    </div>
                                    <div className="caption">
                                        <h3>{post.get('title')}</h3>
                                        <p>{post.get('content')}</p>
                                    </div>
                                </div>
                            </div>
                            <div className="clearfix"></div>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


function mapStateToProps(state) {

    return {
        post: state.posts.getIn(['post'])
    };
}

export default connect(mapStateToProps, null)(PostDetail)