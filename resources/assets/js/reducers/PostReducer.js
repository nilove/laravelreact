import {FETCH_POSTS, FETCH_POSTS_MORE, CHANGE_LOADING,SHOW_DETAIL_POST} from '../actions/index'
import {fromJS} from 'immutable'
const INITIAL_STATE = fromJS({posts: [], pagination: {}, loading: true, post:{}})


export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case FETCH_POSTS:
            return state.setIn(['posts'], fromJS(action.payload.data.data)).setIn(['pagination'], fromJS(action.payload.data.pagination)).setIn(['loading'], false)
        case FETCH_POSTS_MORE:
            return state.updateIn(['posts'], posts => {
                return posts.concat(fromJS(action.payload.data.data))
            }).setIn(['pagination'], fromJS(action.payload.data.pagination)).setIn(['loading'], false)
        case CHANGE_LOADING:
            return state.setIn(['loading'], action.payload)
        case SHOW_DETAIL_POST:
            return state.setIn(['post'], fromJS(action.payload))
        default:
            return state;

    }
}
