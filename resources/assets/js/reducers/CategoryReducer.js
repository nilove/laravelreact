import {FETCH_CATEGORIES} from '../actions/index'
const INITIAL_STATE = fromJS({categories: []})
import {fromJS} from 'immutable'

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case FETCH_CATEGORIES:
            return state.setIn(['categories'], fromJS(action.payload.data))
        default:
            return state;

    }
}
