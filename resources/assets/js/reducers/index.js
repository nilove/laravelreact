import {combineReducers} from 'redux'
import CategoryReducer from './CategoryReducer'
import PostReducer from './PostReducer'
import FilterReducer from './FilterReducer'
const rootReducer = combineReducers({
    categories: CategoryReducer,
    posts: PostReducer,
    filter: FilterReducer
});

export default rootReducer