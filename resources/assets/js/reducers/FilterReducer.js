import {SET_FILTER_TEXT,SET_FILTER_CATEGORY,SET_FILTER_MIN_PRICE,SET_FILTER_MAX_PRICE,SET_FILTER_PRICE} from '../actions/index'
import {fromJS} from 'immutable'
const INITIAL_STATE = fromJS({
    text: '',
    category_id: [],
    price: ''
})


export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case SET_FILTER_TEXT:
            return state.setIn(['text'], fromJS(action.payload))
        case SET_FILTER_CATEGORY:
            return state.setIn(['category_id'], fromJS(action.payload))
        case SET_FILTER_PRICE:
            return state.setIn(['price'], fromJS(action.payload))
        default:
            return state
    }
}
