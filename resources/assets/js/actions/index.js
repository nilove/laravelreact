import axios from 'axios'

export const FETCH_CATEGORIES = 'FETCH_CATEGORIES'
export const FETCH_POSTS = 'FETCH_POSTS'
export const FETCH_POSTS_MORE = 'FETCH_POSTS_MORE'

export const SET_FILTER_TEXT = 'SET_FILTER_TEXT'
export const SET_FILTER_CATEGORY = 'SET_FILTER_CATEGORY'

export const SET_FILTER_PRICE = 'SET_FILTER_PRICE'
export const CHANGE_LOADING = 'CHANGE_LOADING'
export const SHOW_DETAIL_POST = 'SHOW_DETAIL_POST'

export const fetchCategories = () => {
    const request = axios.get(`categories`)
    return {
        type: FETCH_CATEGORIES,
        payload: request
    }
}


export const fetchPosts = (page = 1, query = {}) => {
    query = encodeURI(JSON.stringify(query))
    const request = axios.get(`posts?page=${page}&query=${query}`)
    if (page == 1)
        return {
            type: FETCH_POSTS,
            payload: request
        }
    return {
        type: FETCH_POSTS_MORE,
        payload: request
    }
}


export const setFilterText = (text)=> {
    return {
        type: SET_FILTER_TEXT,
        payload: text
    }
}


export const setFilterCategory = (categories)=> {
    return {
        type: SET_FILTER_CATEGORY,
        payload: categories
    }
}

export const setFilterPrice = (price)=> {
    return {
        type: SET_FILTER_PRICE,
        payload: price
    }
}


export const changeLoading = (loading)=> {
    return {
        type: CHANGE_LOADING,
        payload: loading
    }
}


export const showDetailPost = (post)=> {
    return {
        type: SHOW_DETAIL_POST,
        payload: post
    }
}





