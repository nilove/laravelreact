<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Transformer\PostTransformer;
use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    private $post;

    public function __construct(Post $post)
    {
        $this->post = $post;
    }


    public function index(PostTransformer $postTransformer, Request $request)
    {

        $inputData = $request->all();
        $filter = json_decode($inputData['query'], true);
        $posts = $this->post->where(function ($query) use ($filter) {
            if (isset($filter['search']) && trim($filter['search']) != '')
                $query->where('title', 'like', '%' . trim($filter['search']) . '%');
            if (isset($filter['price']) && $filter['price'] != '')
                $query->whereBetween('price', [explode(',',$filter['price'])]);
            if (isset($filter['category_id']) && !empty($filter['category_id']))
                $query->leftJoin('category_post', function ($join) use ($filter) {
                    $join->on('category_post.post_id', '=', 'posts.id');
                    $join->whereIn('category_post.category_id', $filter['category_id']);
                });
        })->paginate(20);
        if ($posts) {
            $posts = $posts->toArray();
            $pagination = [
                'total' => $posts['total'],
                'per_page' => $posts['per_page'],
                'current_page' => $posts['current_page'],
                'last_page' => $posts['last_page'],
                'next_page_url' => $posts['next_page_url'],
                'prev_page_url' => $posts['prev_page_url'],
                'from' => $posts['from'],
                'to' => $posts['to']
            ];

            return response()->json([
                'data' => $postTransformer->transformCollection($posts),
                'pagination' => $pagination
            ], 200);
        }

    }
}
