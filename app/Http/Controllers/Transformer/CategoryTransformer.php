<?php

namespace App\Http\Controllers\Transformer;


class CategoryTransformer extends Transformer
{
    public function transform($category){
        return [
            'id' => $category['id'],
            'title' => $category['title'],
            'total_post' => count($category['posts'])
        ];
    }
}