<?php
namespace App\Http\Controllers\Transformer;

abstract class Transformer
{
    /**
     * Transform Every Type of Array For Api
     *
     * @param array $items
     * @return array
     */
    public function transformCollection(array $items)
    {
        if (array_key_exists('total', $items) && array_key_exists('data', $items)) {
            $paginateData = $items['data'];
            return array_map([$this, 'transform'], $paginateData);
        } else {
            return array_map([$this, 'transform'], $items);
        }
    }


    /**
     * @param $item
     * @return array
     */
    public abstract function transform($item);
}