<?php
namespace App\Http\Controllers\Transformer;


class PostTransformer extends Transformer
{
    public function transform($post)
    {
        return [
            'id' => $post['id'],
            'title' => $post['title'],
            'content' => $post['content'],
            'thumb' => $post['thumb'],
            'price' => $post['price']
        ];
    }
}