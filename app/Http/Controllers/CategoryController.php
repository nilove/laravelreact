<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Transformer\CategoryTransformer;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    protected $category;

    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    public function index(CategoryTransformer $categoryTransformer)
    {
        return response()->json($categoryTransformer->transformCollection($this->category->with(['posts' => function ($query) {
            $query->select('id');
        }])->get()->toArray()), 200);
    }
}
