<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';
    protected $fillable = ['title', 'content', 'user_id', 'thumb', 'price'];

    public function Categories()
    {
        return $this->belongsToMany(Category::class);
    }
}